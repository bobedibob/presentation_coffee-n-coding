#include "banana.hpp"

#include "catch.hpp"

TEST_CASE("Banana") {
    fubar::bar::Banana banana;
    
    CHECK(std::string(banana) == "banana");
}
