#include "bar/bazinga.hpp"

#include <iostream>

int main() {
    fubar::bar::Bazinga bazinga;
    std::cout << bazinga << std::endl;
    
    return 0;
}
