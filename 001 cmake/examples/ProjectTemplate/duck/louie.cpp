#include "bar/bazinga.hpp"
#include "foo/kung.hpp"

#include <iostream>

int main() {
    fubar::foo::Kung kung;
    fubar::bar::Bazinga bazinga;
    std::cout << kung << " " << bazinga << std::endl;
    
    return 0;
}
