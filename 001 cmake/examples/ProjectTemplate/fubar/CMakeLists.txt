project(Fubar)

add_subdirectory(bar)
add_subdirectory(foo)

add_library(fubar SHARED $<TARGET_OBJECTS:Fubar::Bar::Objects>)
add_library(Fubar ALIAS fubar)

custom__target_compile_defaults(fubar)
custom__target_enable_warnings(fubar)
target_link_libraries(fubar PUBLIC Fubar::Bar Fubar::Foo)
