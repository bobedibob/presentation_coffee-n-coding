#include "foo/kung.hpp"

#include "fu.hpp"

#include "norris.hpp"

namespace fubar {
namespace foo {

Kung::operator std::string() const {
    return std::string(Norris()) + " kung " + std::string(Fu());
}

std::ostream& operator<<(std::ostream& stream, const Kung& kung) {
    stream << std::string(kung);
    return stream;
}

} // foo
} // fubar
