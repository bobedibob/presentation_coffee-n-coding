#include "norris.hpp"
#include "chuck.hpp"

namespace fubar {
namespace foo {

Norris::operator std::string() const {
    return std::string(bar::Chuck()) + " norris";
}

std::ostream& operator<<(std::ostream& stream, const Norris& norris) {
    stream << std::string(norris);
    return stream;
}

} // foo
} // fubar
