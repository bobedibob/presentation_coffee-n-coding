#include "fu.hpp"

namespace fubar {
namespace foo {

Fu::operator std::string() const {
    return "fu";
}

std::ostream& operator<<(std::ostream& stream, const Fu& fu) {
    stream << std::string(fu);
    return stream;
}

} // foo
} // fubar
