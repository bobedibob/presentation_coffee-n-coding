#ifndef _FU_HPP_
#define _FU_HPP_

#include <string>
#include <ostream>

namespace fubar {
namespace foo {

class Fu {
public:
    operator std::string() const;
};

std::ostream& operator<<(std::ostream& stream, const Fu& fu);

} // namespace foo
} // namespace fubar

#endif // _FU_HPP_
