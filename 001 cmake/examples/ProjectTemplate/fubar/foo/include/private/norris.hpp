#ifndef _NORRIS_HPP_
#define _NORRIS_HPP_

#include <string>
#include <ostream>

namespace fubar {
namespace foo {

class Norris {
public:
    operator std::string() const;
};

std::ostream& operator<<(std::ostream& stream, const Norris& norris);

} // namespace foo
} // namespace fubar

#endif // _NORRIS_HPP_
