#ifndef _KUNG_HPP_
#define _KUNG_HPP_

#include <string>
#include <ostream>

namespace fubar {
namespace foo {

class Kung {
public:
    operator std::string() const;
};

std::ostream& operator<<(std::ostream& stream, const Kung& kung);

} // namespace foo
} // namespace fubar

#endif // _KUNG_HPP_
