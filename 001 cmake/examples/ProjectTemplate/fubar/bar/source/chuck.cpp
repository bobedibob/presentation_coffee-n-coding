#include "chuck.hpp"

namespace fubar {
namespace bar {

Chuck::operator std::string() const {
    return "chuck";
}

std::ostream& operator<<(std::ostream& stream, const Chuck& chuck) {
    stream << std::string(chuck);
    return stream;
}

} // bar
} // fubar
