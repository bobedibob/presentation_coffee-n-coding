#include "bar/bazinga.hpp"

#include "banana.hpp"

namespace fubar {
namespace bar {

Bazinga::operator std::string() const {
    return "bazinga " + std::string(Banana());
}

std::ostream& operator<<(std::ostream& stream, const Bazinga& bazinga) {
    stream << std::string(bazinga);
    return stream;
}

} // bar
} // fubar
