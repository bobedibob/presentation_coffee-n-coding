#include "banana.hpp"

namespace fubar {
namespace bar {

Banana::operator std::string() const {
    return "banana";
}

std::ostream& operator<<(std::ostream& stream, const Banana& banana) {
    stream << std::string(banana);
    return stream;
}

} // bar
} // fubar
