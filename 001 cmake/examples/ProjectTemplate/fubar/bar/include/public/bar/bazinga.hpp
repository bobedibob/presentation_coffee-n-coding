#ifndef _BAZINGA_HPP_
#define _BAZINGA_HPP_

#include <string>
#include <ostream>

namespace fubar {
namespace bar {

class Bazinga {
public:
    operator std::string() const;
};

std::ostream& operator<<(std::ostream& stream, const Bazinga& bazinga);

} // namespace bar
} // namespace fubar

#endif // _BAZINGA_HPP_
