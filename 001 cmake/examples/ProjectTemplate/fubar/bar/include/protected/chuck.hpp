#ifndef _CHUCK_HPP_
#define _CHUCK_HPP_ 

#include <string>
#include <ostream>

namespace fubar {
namespace bar {

class Chuck {
public:
    operator std::string() const;
};

std::ostream& operator<<(std::ostream& stream, const Chuck& chuck);

} // namespace bar
} // namespace fubar

#endif // _CHUCK_HPP_
