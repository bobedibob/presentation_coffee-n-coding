#ifndef _BANANA_HPP_
#define _BANANA_HPP_

#include <string>
#include <ostream>

namespace fubar {
namespace bar {

class Banana {
public:
    operator std::string() const;
};

std::ostream& operator<<(std::ostream& stream, const Banana& banana);

} // namespace bar
} // namespace fubar

#endif // _BANANA_HPP_
