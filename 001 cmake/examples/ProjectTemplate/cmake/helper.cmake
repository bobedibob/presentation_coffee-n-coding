
function(custom__target_compile_defaults target)

target_compile_options(${target} PRIVATE -std=c++11)
target_compile_options(${target} PRIVATE -O2 -g3)

endfunction()


function(custom__target_enable_warnings target)

target_compile_options(${target} PRIVATE -W -Wall -Wextra -Wconversion -Wuninitialized -Wno-keyword-macro -pedantic)

endfunction()
