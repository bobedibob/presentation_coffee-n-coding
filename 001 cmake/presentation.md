name: inverse
layout: true
class: center, middle, inverse

---

#Coffee-n-Coding
[modern cmake]
.footnote[guest contribution by Mathias .red[»Bob«] Kraus]

???

What is this talk about?

-> Not a tutorial for absolute beginners but for experienced beginners how to use cmake for modular design.

What is cmake?

-> A build system generator for e.g. make or visual studio.

---

layout: false

.left-column[
  ## How do you cmake?
]
.right-column[

Does this look familiar to you?

```cmake
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -g3")
include_directories(../3rdParty/include)
include_directories(include)
add_library(bar bar.cpp)
```

Do you have such a dependency graph?

```
+--------+               +--------+
|        |  <--------->  |        |
|  FOO   |               |  BAR   |
|        |  <-       ->  |        |
+--------+    \     /    +--------+
    ˄          \   /         ˄
    |           \ /          |
    |            X           |
    |           / \          |
    ˅          /   \         ˅
+--------+    /     \    +--------+
|        |  <-       ->  |        |
|  BAZ   |               |  FRED  |
|        |  <--------->  |        |
+--------+               +--------+
```

]

---

.left-column[
  ## How do you C++?
]
.right-column[

Would you do this in C++?

```c++
int foo;

class Bar {
public:
    void setFoo(int myFoo) {
        foo = myFoo;
    }
    int getFoo() {
        return foo;
    }
};
```
]

--

.right-column[
No? Then why do you do it in cmake?

We should use the same principels for cmake as for the rest of your codebase.
- encapsulation
- separation of concerns
- reusability
- etc.

]

---

.left-column[
  ## Legacy cmake
  ### The don'ts
]
.right-column[

Forget this commands. They operate on directory level and affect all targets.
- include_directories
- link_directories
- link_libraries
- add_compile_options (well, with some exceptions)

Don't use .bold[target_include_libraries] with a path .bold[outside] the .bold[module]. It breaks encapsulation.

Don't use .bold[target_link_libraries] without .bold[PUBLIC], .bold[PRIVATE] or .bold[INTERFACE].

Don't use .bold[target_compile_options] to set flags that affects .bold[ABI] (like -std=c++11).

Don't use file(.bold[GLOB]) in cmake projects, but only in cmake scripts. Why? Because it works on the cmake side, not on the build system side!
- when a new file is added, the CMakeLists.txt is not changed
- therefore, a run of cmake has no effect
- continuing with make results in linker errors ... hooray!
- touching the CMakeLists.txt results in a full rebuild ... hooray!
- pushing to git has no effect to the CMakeLists.txt
- your colleagues get linker errors ... hooray!

]

???

Avoid custom variables and don't use strings.

---

.left-column[
  ## Modern cmake
  ### Brief overview
]
.right-column[

With cmake 2.8.12 (released October 2013) there was a paradigm shift.

Modern cmake is all about targets and properties.

What are .bold[targets]?
- targets are created by .bold[add_library] and .bold[add_executable]

What are .bold[properties]?
- properties are e.g. .bold[include directories] or .bold[link libraries]

```cmake
add_library(Foo source/foo.cpp)
target_include_directories(Foo PUBLIC include)
```

- .bold[Foo] is the target
- .bold[include_directories] is the property
- .bold[PUBLIC] makes the property transitive


]

---

.left-column[
  ## Modern cmake
  ### Usage requirements
]
.right-column[

Since we banned include_directories and link_libraries, we need a way to specify the .bold[usage requirements] of our targets.

```cmake
add_library(Foo source/foo.cpp)
target_include_directories(Foo PUBLIC include)
target_include_directories(Foo PRIVATE source)
```
cmake properties populate two scopes
- INTERFACE
    - properties define the usage requirements of a target
    - properties have an INTERFACE prefix, e.g. INTERFACE_INCLUDE_DIRECTORIES
- PRIVATE
    - properties define the build requirements of a target
    - properties hav no prefix, e.g. INCLUDE_DIRECTORIES

The .bold[PUBLIC], .bold[INTERFACE] and .bold[PRIVATE] keywords specify the scope of the properties
- PUBLIC pupulates the INTERFACE and PRIVATE property scopes
- INTERFACE populates the INTERFACE property scope
- PRIVATE populates the PRIVATE property scope

]

---

.left-column[
  ## Modern cmake
  ### Build requirements
]
.right-column[

Dependencies are specified by target_link_libraries.

```cmake
add_executable(Bar source/main.cpp)
target_include_directories(Bar PRIVATE source)
target_link_libraries(Bar PRIVATE Foo)
```
In this example, Bar receives all INTERFACE properties from Foo, e.g. the INTERFACE_INCLUDE_DIRECTORIES and therefore knows the location of the public headers.

- PUBLIC -> add all properties of the INTERFACE scope of the dependency to the INTERFACE and PRIVATE scope of the target
- INTERFACE -> add all properties of the INTERFACE scope of the dependency to the INTERFACE scope of the target
- PRIVATE -> add all properties of the INTERFACE scope of the dependency to the PRIVATE scope of the target

]

---

.left-column[
  ## Modern cmake
  ### Executables
]
.right-column[

```cmake
cmake_minimum_version(3.5)
project(MyProject VERSION 1.0)

add_executable(bar bar.cpp)
target_include_directories(bar PRIVATE include)

target_compile_options(bar PRIVATE -std=c++11)
target_compile_options(bar PRIVATE -O2 -g3)
target_compile_options(bar PRIVATE -W -Wall -Wextra -pedantic)
target_compile_options(bar PRIVATE -Wconversion -Wuninitialized)

target_link_libraries(bar PRIVATE 3rdPartyLib)
```
]

---

.left-column[
  ## Modern cmake
  ### Libraries
]
.right-column[

```cmake
cmake_minimum_version(3.5)
project(MyProject VERSION 1.0)

add_library(bar bar.cpp)
target_include_directories(bar PUBLIC include)
target_include_directories(bar PRIVATE source)

target_compile_options(bar PRIVATE -std=c++11)
target_compile_options(bar PRIVATE -O2 -g3)
target_compile_options(bar PRIVATE -W -Wall -Wextra -pedantic)
target_compile_options(bar PRIVATE -Wconversion -Wuninitialized)

target_link_libraries(bar PRIVATE 3rdPartyLib)
```
It is possible to specify if the library is a INTERFACE, SHARED, STATIC or OBJECT library.

]

---

.left-column[
  ## Modern cmake
  ### Interface libraries
]
.right-column[

Header only libraries need to be specified with the INTERFACE keyword as there are no sources to be build.

```cmake
add_library(bar INTERFACE)
target_include_directories(bar PUBLIC include/public)
```

As you notice, there are no sources set. Trying to set a source would result in an error.

INTERFACE libraries are one way to create a target for external libraries.

```cmake
add_library(externalLib INTERFACE)
target_include_directories(externalLib INTERFACE ${PATH_TO_HEADER})
target_link_libraries(externalLib INTERFACE ${PATH_TO_LIB})
```

]

---

.left-column[
  ## Modern cmake
  ### Object libraries
]
.right-column[

When object libraries are used, adding the target to target_link_libraries is not sufficient. The target needs also to be added to add_library/add_executable.

```cmake
add_library(bar OBJECT bar.cpp)
target_include_directories(bar PUBLIC include/public)
```

```cmake
add_executable(foo main.cpp $<TARGET_OBJECTS:bar>)
target_link_libraries(foo PRIVATE bar)
```

]

---

.left-column[
  ## Modern cmake
  ### Anology to C++
]
.right-column[

Constructor
- add_executable
- add_library

Member variables
- all target properties

Member functions
- get_target_property()
- set_target_property()
- get_property(TARGET)
- set_property(TARGET)
- target_compile_definitions()
- target_compile_features()
- target_compile_options()
- target_include_directories()
- target_link_libraries()
- target_sources()

]

---

.left-column[
  ## References
]
.right-column[

- [The Ultimate Guide to Modern CMake](https://rix0r.nl/blog/2015/08/13/cmake-guide/)
- [Modern cmake Talk from Meeting C++17](https://www.youtube.com/watch?v=ztrnb-bVVPo)
- [Effective cmake Talk from C++Now 17](https://www.youtube.com/watch?v=bsXLMQ6WgIk)
- [It's Time To Do CMake Right](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)
- [Exporting and importing targets](https://gitlab.kitware.com/cmake/community/wikis/doc/tutorials/Exporting-and-Importing-Targets)

]

---

name: last-page
template: inverse

## That's all folks (for now)!

Slideshow created using [remark](http://github.com/gnab/remark).
